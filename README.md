# gitlab はよいぞ

Visual Studio Code のMarkdownにgitlabの記法を使って 数式を表示できるオプションがあったのでGitLabを使ってみてます。
  
```math
\bigcup^a_{i \in \Gamma }
```

  ```math
  \bigcup^a_{i \in \Gamma } English is ok.
  ```

  ```math
  \bigcup^a_{i \in \Gamma } 日本語もOK?
  ```

  ```math
  \bigcup^a_{i \in \Gamma } \text{English is ok.}
  ```

  ```math
  \bigcup^a_{i \in \Gamma } \text{日本語もOK?}
  ```


    aaa
    bbb

 お砂場って感じなので Sandbox などと書くべきなのかもなぁ。
 
 ```ocaml
 let hoge = 1
 let fuga = "abc"
 let add (a:int) (b:int):int = a + b
 ```

 ```haskell
 -- haskell code
 main = do
   putStrLn "Hello World!"
 ```
 
## test